class Elastic implements Serializable {
    def steps

    Elastic(steps) { //can pass "script" instead which contains more info than steps
        this.steps = steps
    }

    def notifyElastic(args) {
        //keep the operating system check on the lib to avoid duplicate code
        if(steps.isUnix()) {
            steps.sh "${args}" //shell
            steps.echo "unix"
        }
        else{
            steps.bat "${args}" //command line
        }
    }
}